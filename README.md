# Osunosuke_making_vanet

Yunosuke Kodamaの卒業研究概要。
VANETを用いた後続危険車両煽り回避システム。
速度情報共有の技術を用いて後方から接近する、スピード超過車のアラートを行い、交通の流れを良くするシステム。

# Requirement

* sampleファイル詳細

[sampleSpeedTestMobility.cc]
実行を行う際に必要なファイル。実際には home/workspace/ns-3-allinone/ns-3.30/scratch に置く。

docファイル[aodv.h & aodv.rst]
aodvのファイル、触れないほうが良い。

examplesファイル [sampleProtocolScenario.cc]
シナリオファイル

Helperファイル[sample-helper.h & samplehelper.h]
触れないほうが良い。

Modelファイル
[sample-packet.cc & sample-packet.h]
パケットの中身の設定を主に書いてある。

[sample-routing-prtocol.cc & sample-routing-protocol.h]
実行させた際の処理の中身を主に書いてある。

* kodamabahnファイルの詳細

[SUMOトレイスメモ]
sumoのモビリティファイルをNS3で実行できるように行う方法。

[autobahn.sumocfg]
sumoのどのファイルを実行するか決めるもの。

[autobahn.view.xml]
autobahn.sumo.cgfを実行した際の見え方設定

[autobahn60*1.tcl]
６０台の車両時のNS3で実行をするファイル


[autobahn90*1.tcl]
９０台の車両時のNS3で実行をするファイル


[kodama.net.xml]
道路の設定ファイル


[kodama.rou.xml]
車両の設定ファイル

### Installing
* NS3：https://qiita.com/dorapon2000/items/72dcc08eb3c8857f5603 を参考にダウンロード。

* SUMO：https://qiita.com/search?q=SUMO を参考にダウンロード。


# Usage

DEMOの実行方法など、"hoge"の基本的な使い方を説明する

* 環境一覧：Ubuntu 19.04 , NS-3.30 , SUMO 0.12.3

* ①NS3をインストールし、Sampleファイルを home/workspace/ns-3-allinone/ns-3.30/srcに置き、kodamabahnファイルを home に置く。また、SAMPLEファイルに入っている、sampleSpeedTestMobility.ccをScratchファイルに移動する。

* ②Ubuntu上で端末を開きディレクトリーをNS-3.30にする。

* ③実行方法：60台で実行する際は、 ./waf --run "scratch/sampleSpeedTestMobility --traceFile=/home/"username"/kodamabahn/autobahn601.tcl --nodeNum=62 --duration=363.0 --logFile=ns2-mob.log" --vis　を実行し、90台で実行する際は、 ./waf --run "scratch/sampleSpeedTestMobility --traceFile=/home/"username"/kodamabahn/autobahn90*1.tcl --nodeNum=92 --duration=420.0 --logFile=ns2-mob.log" --vis　を実行する。　nodeNumは車両の台数、durationは実行時間を意味する。 


* ④実行を行うとVidualize画面が出力されるのでSimulationボタンを押すと実行する。

# Note

* NS3系：Sampleファイルの配置場所を間違えてしまうと実行ができない。また、ファイルをsrcフォルダにおいた際には、一度 ./waf configure を実行すること。

* SUMO：kodamabahnファイルにSUMOトレイスという名前のファイルがある。このファイルはSUMOの動きをNS3にて表現を行うことのできる.tclファイルを作成する方法が記載されいてる。これを参考にtraceファイルの作成を参考にしてほしい。

# Author

* 作成者：児玉雄之介
* 所属：ネットワークシステム研究室
* E-mail：is0355er@ed.ritsumei.ac.jp
