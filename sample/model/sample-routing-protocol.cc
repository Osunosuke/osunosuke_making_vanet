/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 IITP RAS
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#define Dmin 80  //速度判定に至る距離の差の最小値
#define Vmin 1000000 //速度のしきい値
#define NS_LOG_APPEND_CONTEXT                                   \
  if (m_ipv4) { std::clog << "[node " << m_ipv4->GetObject<Node> ()->GetId () << "] "; }

#define ReceiveLog 1 //1 log 0 lognothing
#define ImageRange 100 //画像処理の距離
#define NodeNum 17//ノード数
#define NotransTime 5//NotransTime 止まっていたら通信をやめさせる
#define CancelAngle 20 //canselAngle　度内に同じノードがあれば近い方だけ取得する
#define DangerSpeed 180 //危険車両とみなすスピード
#define SimTime 400 //シミュレーション時間
#define SimMilliTime 10 //シミュレーション時間×1000

int timing=0;
//int aloha=0;
//int idkioku;
//int xkioku;
//int timekioku;
int braketimes;

#include "sample-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/boolean.h"
#include "ns3/random-variable-stream.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/udp-l4-protocol.h"
#include "ns3/udp-header.h"
#include "ns3/wifi-net-device.h"
#include "ns3/adhoc-wifi-mac.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include <algorithm>
#include <limits>
#include <stdio.h>
#include <map>
#include <cmath>

#include "ns3/mobility-module.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("SampleRoutingProtocol");

namespace sample {
NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);

/// UDP Port for SAMPLE control traffic
const uint32_t RoutingProtocol::SAMPLE_PORT = 654;




RoutingProtocol::RoutingProtocol ()
{
}

TypeId
RoutingProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::sample::RoutingProtocol")
    .SetParent<Ipv4RoutingProtocol> ()
    .SetGroupName ("Sample")
    .AddConstructor<RoutingProtocol> ()
    .AddAttribute ("UniformRv",
                   "Access to the underlying UniformRandomVariable",
                   StringValue ("ns3::UniformRandomVariable"),
                   MakePointerAccessor (&RoutingProtocol::m_uniformRandomVariable),
                   MakePointerChecker<UniformRandomVariable> ())
  ;
  return tid;
}



RoutingProtocol::~RoutingProtocol ()
{
}

void
RoutingProtocol::DoDispose ()
{

}

void
RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
{
  *stream->GetStream () << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
                        << "; Time: " << Now ().As (unit)
                        << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
                        << ", SAMPLE Routing table" << std::endl;
}

int64_t
RoutingProtocol::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
}



Ptr<Ipv4Route>
RoutingProtocol::RouteOutput (Ptr<Packet> p, const Ipv4Header &header,
                              Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
{
  Ptr<Ipv4Route> route;

  if (!p)
    {
	  std::cout << "loopback occured! in routeoutput";
	  return route;// LoopbackRoute (header,oif);
	}

  if (m_socketAddresses.empty ())
    {
	  sockerr = Socket::ERROR_NOROUTETOHOST;
	  NS_LOG_LOGIC ("No zeal interfaces");
	  std::cout << "RouteOutput No zeal interfaces!!, packet drop\n";

	  Ptr<Ipv4Route> route;
	  return route;
    }





  
  return route;
}



bool
RoutingProtocol::RouteInput (Ptr<const Packet> p, const Ipv4Header &header,
                             Ptr<const NetDevice> idev, UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb, LocalDeliverCallback lcb, ErrorCallback ecb)
{
  
  std::cout<<"Route Input Node: "<<m_ipv4->GetObject<Node> ()->GetId ()<<"\n";
  return true;
}



void
RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != 0);
  NS_ASSERT (m_ipv4 == 0);
  m_ipv4 = ipv4;
}

void
RoutingProtocol::NotifyInterfaceUp (uint32_t i)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                        << " interface is up");
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
    {
      return;
    }
  // Create a socket to listen only on this interface
  Ptr<Socket> socket;

  socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvSample,this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetLocal (), SAMPLE_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketAddresses.insert (std::make_pair (socket,iface));


    // create also a subnet broadcast socket
  socket = Socket::CreateSocket (GetObject<Node> (),
                                 UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvSample, this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (iface.GetBroadcast (), SAMPLE_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetIpRecvTtl (true);
  m_socketSubnetBroadcastAddresses.insert (std::make_pair (socket, iface));

  if (m_mainAddress == Ipv4Address ())
    {
      m_mainAddress = iface.GetLocal ();
    }

  NS_ASSERT (m_mainAddress != Ipv4Address ());
}

void
RoutingProtocol::NotifyInterfaceDown (uint32_t i)
{
}

void
RoutingProtocol::NotifyAddAddress (uint32_t i, Ipv4InterfaceAddress address)
{
}

void
RoutingProtocol::NotifyRemoveAddress (uint32_t i, Ipv4InterfaceAddress address)
{ 
}



void
RoutingProtocol::DoInitialize (void)     //ノードの数だけnode0から呼び出される関数 
{
  uint32_t id = m_ipv4->GetObject<Node> ()->GetId ();

  if(id == 0)
  {
    //SendXBroadcast();
//    SetPushBack();
  }
  if(NodeNum>0)   //nod|e id と13までのノード
    {
      for(int i=1; i<SimTime; i++){
//          Simulator::Schedule(Seconds(i), &RoutingProtocol::GetGod, this);
          Simulator::Schedule(Seconds(i), &RoutingProtocol::HelloNodeId, this);

          Simulator::Schedule(Seconds(i), &RoutingProtocol::SetMyPos, this);

          //getareagod メソッドで作られるマップの定期的なクリア
          Simulator::Schedule(Seconds(i), &RoutingProtocol::EraseAngle, this);
      }
      for(int i=1; i<SimMilliTime; i++){
        //何秒かごとの全ノードの位置情報をstratic 変数に保存する
//        Simulator::Schedule(MilliSeconds(i), &RoutingProtocol::SetGodCall, this);
//        Simulator::Schedule(MilliSeconds(i), &RoutingProtocol::GetGodposition, this);

        Simulator::Schedule(Seconds(i), &RoutingProtocol::SetMyPos, this);
      }
      for(int i=1; i<9; i++){
        Simulator::Schedule(Seconds(i), &RoutingProtocol::SetMyPos, this);
      }
    }





}

void
RoutingProtocol::RecvSample (Ptr<Socket> socket)
{

                                timing++;
  //std::cout<<"recv\n";
  Address sourceAddress;
  Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
  uint32_t id = m_ipv4->GetObject<Node> ()->GetId ();
  //std::cout<<"packet size"<<packet->GetSize()<<"\n";
  int x,y; //パケットを受け取った時点のノードの座標
  Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> () ->GetObject<MobilityModel>();
      



      Vector mypos = mobility->GetPosition ();
      x = mypos.x;
      y = mypos.y;

/*

      if(aloha[id]>0){
        if(y==50){
       // std::cout<<"変更前"<<y<<;
        y = y-3;
        //aloha[id]++;
//        std::cout<<"変更後:"<<y<<"\n";
  //      std::cout<<"aloha:"<<aloha<<"\n\n";
      }
      }
*/
      



  TypeHeader tHeader (SAMPLETYPE_HELLOID);//default
  packet->RemoveHeader (tHeader);
  
  if (!tHeader.IsValid ())
    {
      NS_LOG_DEBUG ("Sample protocol message " << packet->GetUid () << " with unknown type received: " << tHeader.Get () << ". Drop");
      return; // drop
    }
  switch (tHeader.Get ())
    {
    case SAMPLETYPE_HELLOID:
    {
      
      if(mtran[id] == 1){//通信可能ノードのみ


//if(id>10){ //////////////////////////////////////////////////////

      IdHeader Idheader;
      packet->RemoveHeader(Idheader);
      uint32_t helloid = Idheader.GetHelloId ();
      int helloxpoint = Idheader.GetHelloXpoint ();
      uint32_t helloypoint = Idheader.GetHelloYpoint ();
      uint32_t hellospeed = Idheader.GetHelloSpeed ();
      int recvidtime = Simulator::Now().GetSeconds();
      int id = m_ipv4->GetObject<Node> ()->GetId ();


/*
      if(aloha[id]>0&&y==50){
       // std::cout<<"変更前"<<y<<;
        y = y-3;
        aloha[id]++;
        std::cout<<"変更後:"<<y<<"\n";
        std::cout<<"aloha:"<<aloha[id]<<"\n\n";

      }

*/



/*
if(mypos.x>50){
      std::cout<<"######Node ["<< id<<"]パケットを受け取りました。########\n";
      std::cout<<"Node["<<id<<"]はNode["<<helloid<<"]からパケットをもらいました。"<<"\n";
      std::cout<<"Node["<<id<<"]座標は("<<x<<","<<y<<")  Node["<<helloid<<"]座標は("<<helloxpoint<<","<<helloypoint<<")\n";
      std::cout<<" 速度:"<<hellospeed<<"\n";
      std::cout<<timing<<"\n";
*/
//int hhelloid = helloid;

      if(hellospeed>110){
        if(/*y==50&&*/helloxpoint<x&&aloha[id]==0&&helloid==91){
          //std::cout<<"スピード超過発見時刻："<<Simulator::Now().GetSeconds()<<"\n"; 
          //std::cout<<"Node["<<id<<"]はNode["<<helloid<<"]からパケットをもらいました。"<<"\n";
          //std::cout<<"速度:"<<hellospeed<<"\n";
          //std::cout<<"Node["<<id<<"]座標は("<<x<<","<<y<<")\n";
          //std::cout<<"超過車Node["<<helloid<<"]座標は("<<helloxpoint<<","<<helloypoint<<")\n";
//          aloha++;
          //std::cout<<"aloha:"<<aloha<<"\n\n";
//          idkioku=id;
          //std::cout<<"id記憶:"<<idkioku<<"\n\n";
          //xkioku=x;
          
          timekioku[id]=Simulator::Now().GetSeconds();
          idkioku[id]=id;
          aloha[id]++;
          std::cout<<"id:"<<idkioku[id]<<"回数："<<aloha[id]<<"\n";
/*          if(kid[id]<1){
kid[id]++;
std::cout<<"id:"<<idkioku<<"回数："<<aloha<<"\n";

          }*/


        }

      if(aloha[id]>0&&idkioku[id]==id&&helloxpoint>x){
        //std::cout<<"追いつきました。\nidは"<<idkioku<<" ";
        //std::cout<<"timekioku:"<<timekioku<<"\n";
        recvidtime=recvidtime-timekioku[id];
 //       std::cout<<"時間差:"<<recvidtime<<"\n\n";
aloha[id]=0;

      }

      }

      //std::cout<<"距離感を比較します。\n"; //(x,y)=自分の座標
      int kyorikan;
      if((helloypoint-y)==0){
         kyorikan = (x-helloxpoint);
          if(kyorikan<0){
            kyorikan = (kyorikan*(-1));
          }
//        std::cout<<"距離感："<<kyorikan<<"\n\n";
        
/*  if(id==31||63||103){        
        if(kyorikan<20&&x>500&&x<5979){
          
          std::cout<<"⚠.危険ブレーキ作動しました.⚠\n\n";  
          braketimes++;
          std::cout<<id<<"\n"<<helloid<<"ブレーキ回数合計:"<<braketimes<<"\n";

          //煽り運転車両のIDを用いてブレーキ回数    if(id==){}


          }
  }
  */
          else{
            
            break;
          }
      }
  //    }
    

      int intrecvidtime = recvidtime;  //８ビットをintに変換
      double dist = std::sqrt((mxpoint[helloid] - helloxpoint) * (mxpoint[helloid] - helloxpoint) 
        + (mypoint[helloid] - helloypoint) * (mypoint[helloid] - helloypoint));

        if(dist>Dmin) //Dminより位置の差が大きい場合
        {
        int timelag = intrecvidtime - mtime[helloid];
        double cotimelag = timelag/100000;
        double speed = dist/cotimelag*10;
        double cospeed = speed*3.6;
          if(cospeed>Vmin){//速度違反なら
            }else{//速度違反じゃないな  
              }
        }else{//Dminより位置の差が小さい場合
        }
        //-------------------speed measument
   //   }////////////////////////////////
      
      break;
      }
      
    }//SAMPLETYPE_HELLOID    
      }//通信可能ノードのみ 

}

void
RoutingProtocol::SendXBroadcast (uint32_t recvid, uint32_t posx ,uint32_t posy, uint32_t speed, uint8_t hopcount,
uint32_t time,uint8_t danger, uint32_t myposx, uint32_t myposy)
{
  
  
  std::cout<<"sendxbroadcast\n\n";
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
      
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();

      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }
      std::cout<<Simulator::Now().GetMicroSeconds()<<"\n";  
      socket->SendTo (packet, 0, InetSocketAddress (destination, SAMPLE_PORT));
      
      std::cout<<"broadcast sent\n"; 
        
     }
  
}


void
RoutingProtocol::HelloNodeId(void)
{
 for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
      uint32_t id = m_ipv4->GetObject<Node> ()->GetId ();//doinitialize  のIDと同じID
      if(mtran[id] == 1){//通信可能ノードのみ

      int posx,posy;

     
      //位置情報を取得
      Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> () ->GetObject<MobilityModel>();

      Vector mypos = mobility->GetPosition ();
      posx = mypos.x;
      posy = mypos.y;

      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();
      

/*      IdHeader idheader(id,posx,posy,speed);
      packet->AddHeader (idheader);
      TypeHeader tHeader (SAMPLETYPE_HELLOID);  
      packet->AddHeader (tHeader);
*/
      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }

if(mypos.x>50){
//      std::cout<< "*Node[" <<id<<"]パケットを送信しました.=";  
//      std::cout<<"送信時刻："<<Simulator::Now().GetMicroSeconds()<<"===\n";  
}
      Time Jitter = Time (MicroSeconds (m_uniformRandomVariable->GetInteger (0,510)));
      //socket->SendTo (packet, 0, InetSocketAddress (destination, SAMPLE_PORT));
      Simulator::Schedule (Jitter, &RoutingProtocol::SendTo,this, socket, packet,destination);

////////////////////////////////////////////////

 if(myxpoint.count(id) == 0){//すでに自分の前の位置がセットされていないとき
   myxpoint[id] = posx;
   myypoint[id] = posy;
  }else{//前の座標のデータがあるとき
   double mydist = std::sqrt((myxpoint[id] - posx) * (myxpoint[id] - posx) 
        + (myypoint[id] - posy) * (mypoint[id] - posy));//ｘ秒前の位置と現在の自分の位置の距離
        //std::cout<<"１秒前からの移動"<<mydist<<"\n";
        double myspeed = mydist*60*60/1000;


double speed = myspeed;

IdHeader idheader(id,posx,posy,speed);
      packet->AddHeader (idheader);
      TypeHeader tHeader (SAMPLETYPE_HELLOID);  
      packet->AddHeader (tHeader);


  }
  if(mypos.x>50){
//    std::cout<<"-パケットの中身----------------------------\n";
//    std::cout<<"Node["<<id<<"]\n";
  }
     }
    }
}//Hellonodeid



void
RoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination){
  socket->SendTo (packet, 0, InetSocketAddress (destination, SAMPLE_PORT));
}


void
RoutingProtocol::ReSendXBroadcast (uint32_t id, uint32_t posx, uint32_t posy, uint32_t speed, uint8_t hopcount, uint32_t time,uint8_t danger )
{
 for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
      //uint32_t id = m_ipv4->GetObject<Node> ()->GetId ();//doinitialize  のIDと同じID
      //std::cout<< "ReSendXBroadCast method id   " <<id<<"\n\n";


      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();


      // Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
      Ipv4Address destination;
      
      if (iface.GetMask () == Ipv4Mask::GetOnes ())
        {
          destination = Ipv4Address ("255.255.255.255");
        }
      else
        {
          destination = iface.GetBroadcast ();
        }
      //std::cout<<Simulator::Now().GetMicroSeconds()<<"\n";  
      socket->SendTo (packet, 0, InetSocketAddress (destination, SAMPLE_PORT));
      
      //std::cout<<"broadcast sent\n"; 
        
     }
}//resendxbroadcast


//////////////////////////////////////////////////////////////////////////////mapmethod
void
RoutingProtocol::SaveXMap(int mapid, int mapxpoint)
{
 
mxpoint[mapid] = mapxpoint;
if(mxpoint.find(1) == mxpoint.end()){
  std::cout<<"not found"<<std::endl;
}else{
  std::cout<<"found"<<std::endl;
}


}//savemap

int
RoutingProtocol::GetXMap(int getid)
{
  return mxpoint[getid];
}

void
RoutingProtocol::SaveYMap(int mapid, int mapypoint)
{
 
mypoint[mapid] = mapypoint;
if(mypoint.find(1) == mypoint.end()){
  std::cout<<"not found"<<std::endl;
}else{
  std::cout<<"found"<<std::endl;
}


}//savemap

int
RoutingProtocol::GetYMap(int getid)
{
  return mypoint[getid];
}

void
RoutingProtocol::SaveTMap(int mapid, int maptime)
{
 
mtime[mapid] = maptime;
if(mtime.find(1) == mtime.end()){
  std::cout<<"not found"<<std::endl;
}else{
  std::cout<<"found"<<std::endl;
}


}//savemap

int
RoutingProtocol::GetTMap(int getid)
{
  return mtime[getid];
}

void
RoutingProtocol::SaveDMap(int mapid, int mapdanger)
{
 
mdanger[mapid] = mapdanger;
if(mdanger.find(1) == mdanger.end()){
  std::cout<<"not found"<<std::endl;
}else{
  std::cout<<"found"<<std::endl;
}


}//savemap

int
RoutingProtocol::GetDMap(int getid)
{
  return mdanger[getid];
}



////////////////////////////////////////////////////////mapmethod

int GetSpeed(int newx,int newy, int newtime, int x,int y, int time){

  double dist = std::sqrt((newx - x) * (newx - x) + (newy - y) * (newy - y));
  double timelag = newtime -  time;
  double speed = dist/timelag;
  return speed;
  std::cout << speed <<"/n";
}

bool has_key_using_count(std::map<int,int> &mxpoint,int n)
{
  if (mxpoint.count(n) == 0){
        std::cout << "m doesn't have " << n << "." << std::endl;
        return false;
    }
    else{
        std::cout << "m has " << n << "." <<std::endl;
        return true;
    }
}

//// ノードが自分の速度を自分で図る　ｘ秒ごとの自分のポジションを保存

void
RoutingProtocol::SetMyPos()//自分の位置を取得してマップに保存
{

  //std::cout<<"setmypos"<<"\n\n";
  int id = m_ipv4->GetObject<Node> ()->GetId ();
  int myposx,myposy;
  Ptr<MobilityModel> mobility = m_ipv4->GetObject<Node> () ->GetObject<MobilityModel>();

   Vector mypos = mobility->GetPosition ();
   myposx = mypos.x;
   myposy = mypos.y;

if(mypos.x>50){
//std::cout<<"myposx:"<<myposx<<"  myposy:"<<myposy<<" ";
}

  if(mtran[id] != 2){//目的地に到着してなかったら

  if(myxpoint.count(id) == 0){//すでに自分の前の位置がセットされていないとき
   myxpoint[id] = myposx;
   myypoint[id] = myposy;
  }else{//前の座標のデータがあるとき
   double mydist = std::sqrt((myxpoint[id] - myposx) * (myxpoint[id] - myposx) 
        + (myypoint[id] - myposy) * (mypoint[id] - myposy));//ｘ秒前の位置と現在の自分の位置の距離
        ////////////////////////////////////x=1のときmydistは秒速に値する
        //std::cout<<"１秒前からの移動"<<mydist<<"\n";
        double myspeed = mydist*60*60/1000;
        if(myspeed > DangerSpeed){
          std::cout<<"速い速度で走っています。車両のIDは"<<id<<"その速度は"<<myspeed<<"\n";
        }
        if(mydist > 0 && mtran[id] == 0){//車両が動き出したら　
          mtran[id]=1;//通信可能にする
          //std::cout<<"通信可能になりました"<<id<<"\n";
        }
        if(mydist == 0 && mtran[id] == 1)//通信可能車両が止まっていたら
        {
          myposcount[id]++;
        }
        if(mydist > 0 && myposcount[id] > 0)//何秒か止まっていたのに再び動き出したら
        {
          myposcount[id]=0;//止まっていた時間を初期化する
        }
        //時速に変換
        if(myposcount[id] > NotransTime && mtran[id] == 1){//通信可能ノードが一定期間停止
          mtran[id]=2;
          //std::cout<<"通信不可能になりました"<<"\n";
        }


   m_myspeed[id] =  myspeed;//自分の速度をマップに保存

if(mypos.x>50){
// std::cout<<"m_myspeed:"<<m_myspeed[id]<<"\n__________________________________________\n";
}

   myxpoint[id] = myposx;//位置情報をマップに保存
   myypoint[id] = myposy;
  }

  }//目的地に到着してなかったら

}


void
RoutingProtocol::GetAngle(double x, double y, double x2, double y2, double recvid)
{

  double radian = atan2(y2 - y,x2 - x);
  double angle = radian * 180 / 3.14159265;
  //uint32_t id = m_ipv4->GetObject<Node> ()->GetId ();
 //std::cout<<"画像処理しているノードのIDは"<<id<<"\n";
  //std::cout<<"角度が求まりました　角度="<<angle<<"\n";
  //imageangleに角度を保存 keyはRecvid
  imageangle[recvid] = angle;
}

void
RoutingProtocol::EraseAngle()
{
  areaangle.clear();
  //
}

double
RoutingProtocol::GetDist(double x, double y, double x2, double y2)
{
  double dist = std::sqrt((x2 - x) * (x2 - x) 
        + (y2 - y) * (y2 - y));
        return dist;
}


void
RoutingProtocol::Direction(double current_x, double current_y,
double source_x, double source_y)
{

}

} //namespace sample
} //namespace ns3
